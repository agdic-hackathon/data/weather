# Weather & Climate

[![](weather.png)](http://worldclim.org/version2)

## Selected Datasets

| Name | Description | Source | Type | Format | Granularity | Time Period | License |
| ---- | ----------- | ------ | ---- | ------ | ----------- | ------------ | ------- |
| [MERRA-2](/MERRA-2) | Weather Data for Cameroon:<br>- Temperature<br>- Precipitation<br>- Snow<br>- Cloud Cover<br>- Air Density<br>(land area & population weighted) | [Renewables.ninja](https://www.renewables.ninja/) & [OPSD](https://open-power-system-data.org/) from [NASA](https://gmao.gsfc.nasa.gov/reanalysis/MERRA-2/) (see below) | Time Series | CSV | Country, Hour | 1980-2016  | [NASA Data & Information Policy](https://science.nasa.gov/earth-science/earth-science-data/data-information-policy)|

### MERRA-2

[The Modern-Era Retrospective Analysis for Research and Applications, version 2](https://gmao.gsfc.nasa.gov/reanalysis/MERRA-2/) (MERRA-2), is the latest atmospheric reanalysis of the modern satellite era produced by NASA’s Global Modeling and Assimilation Office (GMAO). MERRA-2 assimilates observation types not available to its predecessor, MERRA, and includes updates to the Goddard Earth Observing System (GEOS) model and analysis scheme so as to provide a viable ongoing climate analysis beyond MERRA’s terminus.

#### Renewables.ninja & Open Power System Data

[Renewables.ninja](https://www.renewables.ninja/) allows you to run simulations of the hourly power output from wind and solar power plants located anywhere in the world. This tool helps making scientific-quality weather and energy data available to a wider community.

[Open Power System Data](https://open-power-system-data.org/) (OPSD) is a free-of-charge data platform dedicated to electricity system researchers. We collect, check, process, document, and publish data that are publicly available but currently inconvenient to use. The project is a service provider to the modeling community: a supplier of a public good.

### World Clim

[WorldClim](http://www.worldclim.org/) is a set of global climate layers (gridded climate data) with a spatial resolution of about 1 km2. These data can be used for mapping and spatial modeling.

|variable                       |10 minutes|
| -------------------------     | -------- |
|minimum temperature (°C)       |tmin 10m  |
|maximum temperature (°C)       |tmax 10m  |
|average temperature (°C)       |tavg 10m  |
|precipitation (mm)             |prec 10m  |
|solar radiation (kJ m-2 day-1) |srad 10m  |
|wind speed (m s-1)             |wind 10m  |
|water vapor pressure (kPa)     |vapr 10m  |

**These datasets are available only on hard drive, ask to the facilitators for the access.**
